package main

import (
	"gitlab.com/opencity-labs/area-personale/sentinel/server"
)

func main() {
	serverContext := server.InitServerContext()

	serverContext.StartProcess("kafka server", server.StartKafkaServer)
	serverContext.StartProcess("http server", server.StartHttpServer)

	serverContext.Daemonize()
}
