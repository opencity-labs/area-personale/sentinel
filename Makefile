
BASIC_COMPOSE_CONFIG := -f docker-compose.yml
SERVICE_NAME := sentinel
TOPIC_NAME := security

upd:
	docker compose $(BASIC_COMPOSE_CONFIG) up -d

down:
	docker compose $(BASIC_COMPOSE_CONFIG) down --remove-orphans

restart: down upd

enter:
	docker compose $(BASIC_COMPOSE_CONFIG) exec $(SERVICE_NAME) /bin/bash

logs:
	docker compose $(BASIC_COMPOSE_CONFIG) logs -f $(SERVICE_NAME)

build:
	docker compose $(BASIC_COMPOSE_CONFIG) build --no-cache

app-run:
	docker compose $(BASIC_COMPOSE_CONFIG) exec -it sentinel go run main.go

test:
	docker compose $(BASIC_COMPOSE_CONFIG) exec -it sentinel go test ./...

rp-info:
	docker exec -it redpanda-0 rpk cluster info

rp-create-topic:
	docker exec -it redpanda-0 rpk topic create $(TOPIC_NAME)

rp-produce:
	docker exec -it redpanda-0 rpk topic produce --allow-auto-topic-creation $(TOPIC_NAME)