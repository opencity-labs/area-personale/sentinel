# Sentinel

Questo servizio ha lo scopo di monitorare aspetti legati alla sicurezza. La prima versione implementa un controllo sulla posizione geografica da cui gli utenti effettuano il login e notifica eventuali comportamenti anomali.

## Stadio di sviluppo

Il servizio è nello stato di Development 0.1.0

## Inizializzazione dell'ambiente di sviluppo

1. `git clone https://gitlab.com/opencity-labs/area-personale/sentinel.git`
2. `cd sentinel`
3. `docker compose up -d`

## Makefile

Per agevolare l'esecuzione dei comandi docker è stato aggiunto un Makefile con i seguenti comandi:
- upd: avvia i servizi docker in background 
- down: ferma tutti i servizi 
- restart: effettua un `down` e un `upd` in sequenza
- enter: entra all'interno della macchina Go
- logs: mostra i log del servizio Sentinel
- app-build: compila il codice all'interno della macchina Go
- app-run: esegue l'applicazione all'interno della macchina Go
- rp-info: restituisce informazioni sul cluster Redpanda
- rp-create-topic: crea un nuovo topic prestabilito (security) su Redpanda  
- rp-produce: permette di generare in maniera interattiva dei messaggi di prova sul topic security. Basta lanciare il comando, iniziare a digitare un messaggio e premere invio