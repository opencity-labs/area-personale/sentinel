module gitlab.com/opencity-labs/area-personale/sentinel

go 1.22

require (
	github.com/confluentinc/confluent-kafka-go/v2 v2.4.0
	github.com/getsentry/sentry-go v0.28.0
	github.com/go-chi/chi/v5 v5.0.12
	github.com/google/uuid v1.6.0
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.16.0
	github.com/rs/zerolog v1.33.0
	github.com/sethvargo/go-envconfig v1.0.3
	github.com/swaggest/openapi-go v0.2.51
	github.com/swaggest/rest v0.2.66
	github.com/swaggest/swgui v1.8.1
	github.com/swaggest/usecase v1.3.1
)

require (
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/golang/protobuf v1.5.4 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.4 // indirect
	github.com/prometheus/client_model v0.4.0 // indirect
	github.com/prometheus/common v0.42.0 // indirect
	github.com/prometheus/procfs v0.10.1 // indirect
	github.com/rogpeppe/go-internal v1.12.0 // indirect
	github.com/santhosh-tekuri/jsonschema/v3 v3.1.0 // indirect
	github.com/swaggest/form/v5 v5.1.1 // indirect
	github.com/swaggest/jsonschema-go v0.3.70 // indirect
	github.com/swaggest/refl v1.3.0 // indirect
	github.com/vearutop/statigz v1.4.0 // indirect
	golang.org/x/sys v0.18.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	google.golang.org/protobuf v1.33.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
