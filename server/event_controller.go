package server

import (
	"encoding/json"
	"gitlab.com/opencity-labs/area-personale/sentinel/server/app"
	"gitlab.com/opencity-labs/area-personale/sentinel/server/app/infra"

	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
	"gitlab.com/opencity-labs/area-personale/sentinel/server/models"
)

type EventController struct {
	loginSuccessHandler *app.EventLoginSuccessHandler
	sctx                *ServerContext
}

func NewEventController(sctx *ServerContext) *EventController {

	repo := infra.NewInMemoryLoginHistoryRepository()
	geolocation := infra.NewGeolocationServiceCached(sctx.ServerConfig().GeolocationIfconfigUrl)
	eventLoginSuccessHandler := app.NewEventLoginSuccessHandler(geolocation, repo, sctx.ServerConfig().Environment)

	return &EventController{
		loginSuccessHandler: eventLoginSuccessHandler,
		sctx:                sctx,
	}
}

func (c *EventController) HandleEventMessage(event *kafka.Message) {

	parsedEvent := &models.Event{}
	err := json.Unmarshal(event.Value, parsedEvent)
	if err != nil {

		c.sctx.LogKafkaError().Err(err).Msg("Unable to decode event")

		c.sctx.LogKafkaDebugMessage(event).
			Msg("Message received")

	} else {

		if parsedEvent.Action == "user.login_success" {

			c.handleLoginSuccessEvent(event)
		}

	}
}

func (c *EventController) handleLoginSuccessEvent(event *kafka.Message) {

	parsedEvent := &models.LoginSuccess{}
	err := json.Unmarshal(event.Value, parsedEvent)
	if err != nil {

		c.sctx.LogKafkaError().Err(err).Msg("Unable to decode LoginSuccess event")

	} else {

		c.sctx.LogKafkaDebug().Msg("HandleEvent: new LoginSuccess event was received")

		err := c.loginSuccessHandler.HandleEvent(parsedEvent, c.sctx.LogSuspiciousLoginError())
		if err != nil {
			c.sctx.LogKafkaError().Err(err).Msg("Unable to handle LoginSuccess event")
			return
		}
	}
}
