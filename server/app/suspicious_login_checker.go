package app

import (
	"gitlab.com/opencity-labs/area-personale/sentinel/server/app/domain"
)

func SuspiciousLoginChecker(lastLogin *domain.LoginHistoryElement, loginHistory []domain.LoginHistoryElement) {

	if len(loginHistory) < 2 {

		lastLogin.LoginOutcome = domain.NotDeterminable
		return
	}

	for _, login := range loginHistory {

		if (login.LoginOutcome == domain.ValidAccess || login.LoginOutcome == domain.NotDeterminable) &&
			login.Geolocation.Country != lastLogin.Geolocation.Country {

			lastLogin.LoginOutcome = domain.SuspiciousAccess

			// TODO Se esci da Asy è sospetto
			// TODO Se entri di notte è DangerousAccess e cambia il paese da bloccare
			// TODO Aggiungere le metriche

			return
		}

		lastLogin.LoginOutcome = domain.ValidAccess
	}
}
