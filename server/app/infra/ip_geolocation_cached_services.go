package infra

import (
	"gitlab.com/opencity-labs/area-personale/sentinel/server/app/domain"
	"net"
)

type GeolocationServiceCached struct {
	cache              map[string]domain.Geolocation
	geolocationService *GeolocationService
}

func NewGeolocationServiceCached(serviceUrl string) *GeolocationServiceCached {

	geolocationService := NewGeolocationService(serviceUrl)

	return &GeolocationServiceCached{
		cache:              make(map[string]domain.Geolocation),
		geolocationService: geolocationService,
	}
}

func (i *GeolocationServiceCached) IpGeolocationServices(ip net.IP) (domain.Geolocation, error) {

	geolocationCached, ok := i.cache[ip.String()]

	if ok {
		return geolocationCached, nil
	}

	geolocation, err := i.geolocationService.IpGeolocationServices(ip)
	if err != nil {
		return domain.Geolocation{}, err
	}

	i.cache[ip.String()] = geolocation

	return geolocation, nil
}
