package infra

import (
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/opencity-labs/area-personale/sentinel/server/app/domain"
	"io"
	"net"
	"net/http"
)

type GeolocationService struct {
	serviceUrl string
}

func NewGeolocationService(serviceUrl string) *GeolocationService {

	return &GeolocationService{
		serviceUrl: serviceUrl,
	}
}

func (i *GeolocationService) IpGeolocationServices(ip net.IP) (domain.Geolocation, error) {

	geolocation := domain.Geolocation{}
	address := fmt.Sprintf(i.serviceUrl, ip.String())

	client := http.Client{}
	res, err := client.Get(address)

	if err != nil {

		return geolocation, errors.Errorf("Unable to contact the Geolocation service %s", address)
	}

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return geolocation, err
	}

	err = res.Body.Close()
	if err != nil {
		return domain.Geolocation{}, err
	}

	if res.StatusCode > 299 {
		return geolocation, errors.Errorf("Response failed with status code: %d", res.StatusCode)
	}

	err = json.Unmarshal(body, &geolocation)
	if err != nil {
		return geolocation, err
	}

	return geolocation, nil
}
