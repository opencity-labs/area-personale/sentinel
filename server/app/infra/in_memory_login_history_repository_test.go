package infra

import (
	"fmt"
	"gitlab.com/opencity-labs/area-personale/sentinel/server/app/domain"
	"gitlab.com/opencity-labs/area-personale/sentinel/server/models"
	"reflect"
	"testing"
	"time"
)

func TestInMemoryLoginHistoryRepository_Save(t *testing.T) {

	repo := NewInMemoryLoginHistoryRepository()
	userId := domain.UserId("001")

	elementToSave := createLoginHistoryElement(userId, 1)

	if got := repo.Save(elementToSave); got != nil {
		t.Errorf("Save() = %v", got)
	}
}

func TestInMemoryLoginHistoryRepository_Save_should_remove_old_elements(t *testing.T) {

	repo := NewInMemoryLoginHistoryRepository()
	userId := domain.UserId("001")

	elementToSave1 := createLoginHistoryElement(userId, 0)
	elementToSave2 := createLoginHistoryElement(userId, 1)
	elementToSave3 := createLoginHistoryElement(userId, 2)
	elementToSave4 := createLoginHistoryElement(userId, 3)
	elementToSave5 := createLoginHistoryElement(userId, 4)
	elementToSave6 := createLoginHistoryElement(userId, 5)

	repo.Save(elementToSave1)
	repo.Save(elementToSave2)
	repo.Save(elementToSave3)
	repo.Save(elementToSave4)
	repo.Save(elementToSave5)
	repo.Save(elementToSave6)

	if got := repo.GetByUserId(userId); len(got) != 5 {
		t.Errorf("GetByUserId() = %v", got)
	}

	if got := repo.GetByUserId(userId); !reflect.DeepEqual(got[4], elementToSave6) {
		t.Errorf("GetByUserId() = %v, want %v", got, elementToSave6)
	}
}

func TestInMemoryLoginHistoryRepository_GetByUserId(t *testing.T) {

	repo := NewInMemoryLoginHistoryRepository()
	userId := domain.UserId("001")

	var want []domain.LoginHistoryElement

	want = append(want, createLoginHistoryElement(userId, 1))
	want = append(want, createLoginHistoryElement(userId, 2))

	repo.Save(createLoginHistoryElement(userId, 1))
	repo.Save(createLoginHistoryElement(userId, 2))

	if got := repo.GetByUserId(userId); !reflect.DeepEqual(got, want) {
		t.Errorf("GetByUserId() = %v, want %v", got, want)
	}

	history := repo.GetByUserId(userId)

	fmt.Println(history)
}

func createLoginHistoryElement(userId domain.UserId, dayShift int) domain.LoginHistoryElement {

	occurredAt, _ := time.Parse(time.DateTime, "2024-01-01 19:19:19")
	occurredAt = occurredAt.AddDate(0, 0, dayShift)

	return domain.LoginHistoryElement{
		UserId:       userId,
		LoginOutcome: domain.NotDeterminable,
		OccurredAt: models.Time{
			Time: occurredAt,
		},
	}

}
