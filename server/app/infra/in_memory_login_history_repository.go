package infra

import (
	"gitlab.com/opencity-labs/area-personale/sentinel/server/app/domain"
	"sync"
)

type InMemoryLoginHistoryRepository struct {
	sync.RWMutex
	maxElements int
	data        map[domain.UserId][]domain.LoginHistoryElement
}

func NewInMemoryLoginHistoryRepository() *InMemoryLoginHistoryRepository {

	return &InMemoryLoginHistoryRepository{
		data:        make(map[domain.UserId][]domain.LoginHistoryElement),
		maxElements: 5,
	}
}

func (repo *InMemoryLoginHistoryRepository) Save(loginHistoryElement domain.LoginHistoryElement) error {
	repo.Lock()
	defer repo.Unlock()

	loginHistory := repo.data[loginHistoryElement.UserId]

	if len(loginHistory) >= repo.maxElements {

		loginHistory = loginHistory[1:]
	}

	repo.data[loginHistoryElement.UserId] = append(loginHistory, loginHistoryElement)

	return nil
}

func (repo *InMemoryLoginHistoryRepository) GetByUserId(userId domain.UserId) []domain.LoginHistoryElement {
	repo.RLock()
	defer repo.RUnlock()

	loginHistory, ok := repo.data[userId]

	if ok {
		return loginHistory
	}

	return nil
}
