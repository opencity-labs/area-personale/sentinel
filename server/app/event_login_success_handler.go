package app

import (
	"github.com/rs/zerolog"
	"gitlab.com/opencity-labs/area-personale/sentinel/server/app/domain"
	"gitlab.com/opencity-labs/area-personale/sentinel/server/metrics"
	"gitlab.com/opencity-labs/area-personale/sentinel/server/models"
	"net"
)

type EventLoginSuccessHandler struct {
	geolocation domain.GeolocationService
	repository  domain.LoginHistoryRepository
	environment string
}

func NewEventLoginSuccessHandler(
	geolocation domain.GeolocationService,
	repository domain.LoginHistoryRepository,
	environment string,
) *EventLoginSuccessHandler {

	return &EventLoginSuccessHandler{
		geolocation: geolocation,
		repository:  repository,
		environment: environment,
	}
}

func (i *EventLoginSuccessHandler) HandleEvent(event *models.LoginSuccess, log *zerolog.Event) error {

	geolocation := domain.Geolocation{}

	if ip := net.ParseIP(event.Origin.Ip); ip != nil {

		var errors error = nil
		geolocation, errors = i.geolocation.IpGeolocationServices(ip)

		if errors != nil {

			metrics.ServiceGeolocationFailedProcessed.WithLabelValues(
				i.environment,
			).Inc()

			return errors
		}
	}

	metrics.ServiceGeolocationSuccessProcessed.WithLabelValues(
		i.environment,
	).Inc()

	userId := domain.UserId(event.Actor.UserId)
	loginHistory := i.repository.GetByUserId(userId)

	lastLogin := domain.LoginHistoryElement{
		UserId:      userId,
		EventId:     event.EventId,
		Geolocation: geolocation,
		OccurredAt:  event.EventCreatedAt,
	}

	SuspiciousLoginChecker(&lastLogin, loginHistory)

	if lastLogin.LoginOutcome != domain.ValidAccess && lastLogin.LoginOutcome != domain.NotDeterminable {

		log.
			Str("UserId", string(lastLogin.UserId)).
			Str("LoginSuccessEventId", lastLogin.EventId).
			Str("Country", lastLogin.Geolocation.Country).
			Str("AS", lastLogin.Geolocation.Asn).
			Str("LoginOutcome", lastLogin.Geolocation.Asn).
			Msg("Suspicious login detected")

		metrics.SuspiciousAccess.WithLabelValues(
			i.environment,
			event.TenantID,
		).Inc()
	}

	_ = i.repository.Save(lastLogin)

	return nil
}
