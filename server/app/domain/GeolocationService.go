package domain

import "net"

type GeolocationService interface {
	IpGeolocationServices(ip net.IP) (Geolocation, error)
}
