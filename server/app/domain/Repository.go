package domain

type LoginHistoryRepository interface {
	Save(loginHistory LoginHistoryElement) error
	GetByUserId(userId UserId) []LoginHistoryElement
}
