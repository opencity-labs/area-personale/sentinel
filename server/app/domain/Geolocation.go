package domain

type Geolocation struct {
	Ip      string  `json:"ip"`
	Country string  `json:"country_iso"`
	Region  string  `json:"region_name"`
	Lat     float32 `json:"latitude"`
	Long    float32 `json:"longitude"`
	Asn     string  `json:"asn"`
}
