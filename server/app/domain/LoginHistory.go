package domain

import (
	"gitlab.com/opencity-labs/area-personale/sentinel/server/models"
)

const (
	ValidAccess      = "ValidAccess"
	SuspiciousAccess = "SuspiciousAccess"
	DangerousAccess  = "DangerousAccess"
	NotDeterminable  = "NotDeterminable"
)

type UserId string

type LoginHistoryElement struct {
	EventId      string
	UserId       UserId
	LoginOutcome string
	OccurredAt   models.Time
	Geolocation  Geolocation
}
