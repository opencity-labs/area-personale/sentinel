package server

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/pkgerrors"
	"os"
)

type Logger struct {
	outlog zerolog.Logger
	errlog zerolog.Logger
}

func NewLogger() Logger {
	zerolog.ErrorStackMarshaler = pkgerrors.MarshalStack

	errlog := zerolog.New(os.Stderr).Level(zerolog.ErrorLevel).With().Str("channel", "err").Logger()
	outlog := zerolog.New(os.Stdout).Level(zerolog.InfoLevel).With().Str("channel", "out").Logger()

	return Logger{
		outlog: outlog,
		errlog: errlog,
	}
}
