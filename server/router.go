package server

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/swaggest/rest/web"
	"github.com/swaggest/swgui/v5emb"
)

func registerRoutes(sctx *ServerContext, service *web.Service) {
	service.OpenAPISchema().SetTitle("Sentinel API")
	service.OpenAPISchema().SetDescription("...")
	service.OpenAPISchema().SetVersion("v" + VERSION)

	service.Get("/status", GetStatus(sctx))

	service.Method(http.MethodGet, "/metrics", promhttp.Handler())

	service.Docs("/docs", v5emb.New)
}
