package server

import (
	sentryhttp "github.com/getsentry/sentry-go/http"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/swaggest/openapi-go/openapi3"
	"github.com/swaggest/rest/response/gzip"
	"github.com/swaggest/rest/web"
)

func initService(sctx *ServerContext) *web.Service {
	serverConfig := sctx.ServerConfig()

	service := web.NewService(openapi3.NewReflector())

	service.Wrap(
		gzip.Middleware,
	)

	if serverConfig.SentryEnabled {

		middlewareSentry := sentryhttp.New(sentryhttp.Options{
			Repanic: true,
		})

		service.Use(
			middleware.RealIP,
			middleware.RequestID,
			middleware.CleanPath,
			middleware.StripSlashes,
			middleware.NoCache,
			middleware.RequestLogger(&httpLoggerFormatter{sctx}),
			middleware.Recoverer,
			// middleware.Timeout(60 * time.Second),
			middlewareSentry.Handle,
		)

	} else {

		service.Use(
			middleware.RealIP,
			middleware.RequestID,
			middleware.CleanPath,
			middleware.StripSlashes,
			middleware.NoCache,
			middleware.RequestLogger(&httpLoggerFormatter{sctx}),
			// middleware.Recoverer,
			// middleware.Timeout(60 * time.Second),
		)

	}

	return service
}
