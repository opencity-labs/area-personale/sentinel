package models

type LoginSuccess struct {
	Action         string  `json:"action"`
	TenantID       string  `json:"tenant_id" required:"true"`
	EventId        string  `json:"event_id" required:"true"`
	EventVersion   int8    `json:"event_version" required:"true"`
	EventCreatedAt Time    `json:"event_created_at" required:"true"` // Validazione ISO 8601
	Actor          *Actor  `json:"actor"`
	Origin         *Origin `json:"origin"`
}

type Actor struct {
	SessionId string `json:"session_id"`
	UserId    string `json:"user_id"`
}

type Origin struct {
	Ip string `json:"ip"`
}
