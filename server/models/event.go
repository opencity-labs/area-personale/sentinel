package models

type Event struct {
	Action       string `json:"action"`
	TenantID     string `json:"tenant_id" required:"true"`
	EventId      string `json:"event_id" required:"true"`
	EventVersion int8   `json:"event_version" required:"true"`

	EventCreatedAt Time `json:"event_created_at" required:"true"` // Validazione ISO 8601
}
