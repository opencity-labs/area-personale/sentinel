package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	SuspiciousAccess = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Name: "oc_sentinel_suspicious_access_total",
			Help: "The total number of suspicious access attempts",
		},
		[]string{"env", "tenant"},
	)

	ServiceGeolocationSuccessProcessed = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Name: "oc_sentinel_successfully_geolocation_processed_total",
			Help: "The total number of successfully geolocation processed",
		},
		[]string{"env"},
	)

	ServiceGeolocationFailedProcessed = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Name: "oc_sentinel_failed_geolocation_processed_total",
			Help: "The total number of failed geolocation processed",
		},
		[]string{"env"},
	)
)
