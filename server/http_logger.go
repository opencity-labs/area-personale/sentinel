package server

import (
	"net/http"
	"time"

	"github.com/go-chi/chi/v5/middleware"
)

type httpLoggerFormatter struct {
	sctx *ServerContext
}

func (formatter *httpLoggerFormatter) NewLogEntry(request *http.Request) middleware.LogEntry {
	entry := &httpLoggerEntry{
		sctx:    formatter.sctx,
		request: request,
	}

	return entry
}

type httpLoggerEntry struct {
	sctx    *ServerContext
	request *http.Request
}

func (entry *httpLoggerEntry) Write(status, bytes int, header http.Header, elapsed time.Duration, extra interface{}) {
	milliseconds := float64(elapsed) / float64(time.Millisecond)
	duration := float64(int(milliseconds*100+.5)) / 100

	if status >= 500 {
		entry.sctx.LogHttpError().
			Str("request_id", middleware.GetReqID(entry.request.Context())).
			Str("client_ip", entry.request.RemoteAddr).
			Str("referrer", entry.request.Referer()).
			Str("host", entry.request.Host).
			Float64("duration", duration).
			Str("method", entry.request.Method).
			Str("path", entry.request.RequestURI).
			Int("status", status).
			Msg("")
	} else {
		entry.sctx.LogHttpInfo().
			Str("request_id", middleware.GetReqID(entry.request.Context())).
			Str("client_ip", entry.request.RemoteAddr).
			Str("referrer", entry.request.Referer()).
			Str("host", entry.request.Host).
			Float64("duration", duration).
			Str("method", entry.request.Method).
			Str("path", entry.request.RequestURI).
			Int("status", status).
			Msg("")
	}
}

func (entry *httpLoggerEntry) Panic(v interface{}, stack []byte) {
	entry.sctx.LogHttpFatal().Stack().Msg(string(stack))
}
