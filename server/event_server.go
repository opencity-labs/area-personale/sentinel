package server

import (
	"time"

	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
)

func StartKafkaServer(sctx *ServerContext) {
	serverConfig := sctx.ServerConfig()

	consumer, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": serverConfig.KafkaBootstrapServers,
		"group.id":          serverConfig.KafkaGroupId,
		"auto.offset.reset": "earliest",
		// "go.delivery.reports":    false,
		"go.logs.channel.enable": true,
		"debug":                  ",",
	})
	if err != nil {
		sctx.LogKafkaFatal().Stack().Err(err).Msg("kafka consumer initialization failed")
	}

	// Drain Kafka service logs without printing them
	go func() {
		sctx.LogKafkaDebug().Msg("kafka logs drainer started")
		logsChan := consumer.Logs()
		for {
			select {
			case <-sctx.Done():
				sctx.LogKafkaDebug().Msg("kafka logs drainer received exit signal, shutting down")
				return
			case <-logsChan:
				// fmt.Println("KAFKA LOG RECEIVED AND DRAINED")
			}
		}
	}()

	eventController := NewEventController(sctx)
	consumer.Subscribe(serverConfig.KafkaTopicName, nil)

	for {
		select {
		case <-sctx.Done():
			sctx.LogKafkaDebug().Msg("kafka server received exit signal, shutting down")
			consumer.Close()
			sctx.LogHttpDebug().Msg("kafka server shutted down")
			return

		default:
			ev, err := consumer.ReadMessage(time.Second)

			if err == nil {

				eventController.HandleEventMessage(ev)

			} else if !err.(kafka.Error).IsTimeout() {
				// The client will automatically try to recover from all errors.
				// Timeout is not considered an error because it is raised by
				// ReadMessage in absence of messages.
				sctx.LogKafkaDebug().Msgf("Consumer error: %v (%v)\n", err, ev)
			}
		}
	}
}

func startEventsProducer(sctx *ServerContext) *kafka.Producer {
	serverConfig := sctx.ServerConfig()

	producer, err := kafka.NewProducer(&kafka.ConfigMap{
		"bootstrap.servers":      serverConfig.KafkaBootstrapServers,
		"go.delivery.reports":    false,
		"go.logs.channel.enable": true,
		"debug":                  ",",
	})
	if err != nil {
		sctx.LogKafkaFatal().Stack().Err(err).Msg("kafka producer initialization failed")
	}

	return producer
}
