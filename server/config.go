package server

import (
	"context"

	"github.com/sethvargo/go-envconfig"
)

type ServerConfig struct {
	// Proxy config
	Environment string `env:"ENVIRONMENT,default=local"`
	Debug       bool   `env:"DEBUG,default=true"`
	// Sentry config
	SentryEnabled bool   `env:"SENTRY_ENABLED,default=false"`
	SentryToken   string `env:"SENTRY_DSN,default=test"`

	// Kafka config
	KafkaBootstrapServers string `env:"KAFKA_BOOTSTRAP_SERVERS,default=localhost:9092"`
	KafkaGroupId          string `env:"KAFKA_GROUP_ID,default=sentinel"`
	KafkaTopicName        string `env:"KAFKA_TOPIC_NAME,default=security"`

	// HTTP config
	HttpBind string `env:"HTTP_BIND,default=0.0.0.0"`
	HttpPort string `env:"HTTP_PORT,default=40019"`

	// Geolocation config
	GeolocationIfconfigUrl string `env:"GEOLOCATION_IFCONFIG_URL,default=https://ifconfig.co/json?ip=%s"`
}

func LoadConfig(ctx context.Context) (serverConfig ServerConfig, err error) {
	err = envconfig.Process(ctx, &serverConfig)
	return
}
